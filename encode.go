package nbt

import (
	"compress/gzip"
	"compress/zlib"
	"encoding/binary"
	"fmt"
	"io"
	"reflect"
)

func Marshal(compression Compression, order binary.ByteOrder, out io.Writer, v interface{}) (err error) {
	defer func() {
		if r := recover(); r != nil {
			if s, ok := r.(string); ok {
				err = fmt.Errorf(s)
			} else {
				err = r.(error)
			}
		}
	}()

	if out == nil {
		panic(fmt.Errorf("nbt: Output stream is nil"))
	}

	switch compression {
	case Uncompressed:
		break
	case GZip:
		w := gzip.NewWriter(out)
		defer w.Close()
		out = w
	case ZLib:
		w := zlib.NewWriter(out)
		defer w.Close()
		out = w
	default:
		panic(fmt.Errorf("nbt: Unknown compression type: %d", compression))
	}

	new(encodeState).init(order, out).writeRootTag(reflect.ValueOf(v))

	return
}

type encodeState struct {
	out   io.Writer
	order binary.ByteOrder
}

func (e *encodeState) init(order binary.ByteOrder, out io.Writer) *encodeState {
	e.out = out
	e.order = order
	return e
}

func (e *encodeState) writeRootTag(v reflect.Value) {
	e.writeTag("", v)
}

func (e *encodeState) w(v interface{}) {
	err := binary.Write(e.out, e.order, v)
	if err != nil {
		panic(err)
	}
}

func (e *encodeState) writeTag(name string, v reflect.Value) {
	v = reflect.Indirect(v)
	defer func() {
		if r := recover(); r != nil {
			panic(fmt.Errorf("%v\n\t\tat struct field %#v", r, name))
		}
	}()
	for v.Kind() == reflect.Interface {
		v = v.Elem()
	}
	switch v.Kind() {
	case reflect.Bool:
		e.w(TAG_Byte)
		e.writeValue(TAG_String, name)
		if v.Bool() {
			e.writeValue(TAG_Byte, byte(1))
		} else {
			e.writeValue(TAG_Byte, byte(0))
		}

	case reflect.Int8:
		e.w(TAG_Byte)
		e.writeValue(TAG_String, name)
		e.writeValue(TAG_Byte, int8(v.Int()))

	case reflect.Uint8:
		e.w(TAG_Byte)
		e.writeValue(TAG_String, name)
		e.writeValue(TAG_Byte, uint8(v.Uint()))

	case reflect.Int16:
		e.w(TAG_Short)
		e.writeValue(TAG_String, name)
		e.writeValue(TAG_Short, int16(v.Int()))

	case reflect.Uint16:
		e.w(TAG_Short)
		e.writeValue(TAG_String, name)
		e.writeValue(TAG_Short, uint16(v.Uint()))

	case reflect.Int32:
		e.w(TAG_Int)
		e.writeValue(TAG_String, name)
		e.writeValue(TAG_Int, int32(v.Int()))

	case reflect.Uint32:
		e.w(TAG_Int)
		e.writeValue(TAG_String, name)
		e.writeValue(TAG_Int, uint32(v.Uint()))

	case reflect.Int64:
		e.w(TAG_Long)
		e.writeValue(TAG_String, name)
		e.writeValue(TAG_Long, v.Int())

	case reflect.Uint64:
		e.w(TAG_Long)
		e.writeValue(TAG_String, name)
		e.writeValue(TAG_Long, v.Uint())

	case reflect.Float32:
		e.w(TAG_Float)
		e.writeValue(TAG_String, name)
		e.writeValue(TAG_Float, float32(v.Float()))

	case reflect.Float64:
		e.w(TAG_Double)
		e.writeValue(TAG_String, name)
		e.writeValue(TAG_Double, v.Float())

	case reflect.String:
		e.w(TAG_String)
		e.writeValue(TAG_String, name)
		e.writeValue(TAG_String, v.String())

	case reflect.Array:
		switch v.Type().Elem().Kind() {
		case reflect.Uint8:
			e.w(TAG_Byte_Array)
			e.writeValue(TAG_String, name)
			e.writeValue(TAG_Byte_Array, v.Slice(0, v.Len()).Bytes())

		case reflect.Int32, reflect.Uint32:
			e.w(TAG_Int_Array)
			e.writeValue(TAG_String, name)
			for i := 0; i < v.Len(); i++ {
				e.writeValue(TAG_Int, v.Index(i).Interface())
			}

		default:
			panic(fmt.Errorf("nbt: Unhandled array type: %v", v.Type().Elem()))
		}

	case reflect.Slice:
		e.w(TAG_List)
		e.writeValue(TAG_String, name)
		e.writeList(v)

	case reflect.Map:
		e.w(TAG_Compound)
		e.writeValue(TAG_String, name)
		e.writeMap(v)

	case reflect.Struct:
		e.w(TAG_Compound)
		e.writeValue(TAG_String, name)
		e.writeCompound(v)

	default:
		panic(fmt.Errorf("nbt: Unhandled type: %v (%v)", v.Type(), v.Interface()))
	}
}

func (e *encodeState) writeValue(tag Tag, v interface{}) {
	switch tag {
	case TAG_Byte, TAG_Short, TAG_Int, TAG_Long, TAG_Float, TAG_Double:
		e.w(v)

	case TAG_String:
		e.w(uint16(len(v.(string))))
		_, err := e.out.Write([]byte(v.(string)))
		if err != nil {
			panic(err)
		}

	case TAG_Byte_Array:
		e.w(uint32(len(v.([]byte))))
		_, err := e.out.Write(v.([]byte))
		if err != nil {
			panic(err)
		}

	default:
		panic(fmt.Errorf("nbt: Unhandled tag: %s (%v)", tag, v))
	}
}

func (e *encodeState) writeList(v reflect.Value) {
	var tag Tag
	mustConvertBool := false
	mustConvertMap := false
	switch v.Type().Elem().Kind() {
	case reflect.Bool:
		mustConvertBool = true
		fallthrough
	case reflect.Int8, reflect.Uint8:
		tag = TAG_Byte

	case reflect.Int16, reflect.Uint16:
		tag = TAG_Short

	case reflect.Int32, reflect.Uint32:
		tag = TAG_Int

	case reflect.Int64, reflect.Uint64:
		tag = TAG_Long

	case reflect.Float32:
		tag = TAG_Float

	case reflect.Float64:
		tag = TAG_Double

	case reflect.String:
		tag = TAG_String

	case reflect.Array:
		switch v.Type().Elem().Elem().Kind() {
		case reflect.Uint8:
			tag = TAG_Byte_Array

		case reflect.Int32, reflect.Uint32:
			tag = TAG_Int_Array

		default:
			panic(fmt.Errorf("nbt: Unhandled array type: %v", v.Type().Elem().Elem()))
		}

	case reflect.Slice:
		tag = TAG_List

	case reflect.Map:
		mustConvertMap = true
		fallthrough
	case reflect.Struct:
		tag = TAG_Compound

	case reflect.Ptr: // TODO: Is there ever a case where TAG_Compound would be wrong here?
		tag = TAG_Compound

	default:
		panic(fmt.Errorf("nbt: Unhandled list element type: %v", v.Type().Elem()))
	}
	e.w(tag)
	e.w(uint32(v.Len()))

	var i int
	defer func() {
		if r := recover(); r != nil {
			panic(fmt.Errorf("%v\n\t\tat list index %d", r, i))
		}
	}()
	for i = 0; i < v.Len(); i++ {
		if mustConvertBool {
			if v.Index(i).Bool() {
				e.writeValue(TAG_Byte, uint8(1))
			} else {
				e.writeValue(TAG_Byte, uint8(0))
			}
		} else if tag == TAG_Compound {
			if mustConvertMap {
				e.writeMap(v.Index(i))
			} else {
				e.writeCompound(reflect.Indirect(v.Index(i)))
			}
		} else if tag == TAG_List {
			e.writeList(v.Index(i))
		} else if tag == TAG_Byte_Array {
			e.writeValue(tag, v.Index(i).Bytes())
		} else if tag == TAG_Int_Array {
			for j := 0; j < v.Index(i).Len(); j++ {
				e.writeValue(TAG_Int, v.Index(i).Index(j).Interface())
			}
		} else {
			e.writeValue(tag, v.Index(i).Interface())
		}
	}
}

func (e *encodeState) writeMap(v reflect.Value) {
	for _, name := range v.MapKeys() {
		e.writeTag(name.String(), reflect.Indirect(v.MapIndex(name)))
	}
	e.w(TAG_End)
}

func (e *encodeState) writeCompound(v reflect.Value) {
	v = reflect.Indirect(v)
	fields := parseStruct(v)

	for name, value := range fields {
		e.writeTag(name, value)
	}
	e.w(TAG_End)
}
